# How The Code Works
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/Screenshot_1.png)
<br>
The following point will explain how the code works :
* [Packets](#packets)
* [Packet Handler](#packets-handler)
* [Login](#login)
* [Spawn](#spawn)
* [Movement](#movement)
* [Countdown](#countdown)
* [Role Assign](#role-assign)
* [Game Result](#game-result)
* [Chat System](#chat-system)
* [Item Spawner](#item-spawner)
* [Pick Up Power Up](#pick-up-power-up)
* [Shield Expiration](#shield-expiration)

# Packets
---
- **Server Packets**<br>
This is the declaration of public enum for server packets. Server packets is used a label to a packet that coming from server
```C#=1
public enum ServerPackets
{
    welcome = 1,
    loginResult,
    spawnPlayer,
    playerPosition,
    playerRotation,
    playerTransform,
    playerDisconnected,
    serverSimulationState,
    serverConvar,
    serverTick,
    serverCountdown,
    playersRole,
    gameResult,
    broadcastChat,
    createItemSpawner,
    itemSpawned,
    itemPickedUp,
    shieldExpired,
    turboEnd,
    clientCount,
    gameCountdown,
    endCountdown
}
```
- **Client Packets**<br>
This is the declaration of public enum for client packets. Client packets is used a label to a packet that coming from server
```C#=1
public enum ClientPackets
{
    LoginData = 1,
    playerInput,
    playerConvar,
    chatData
}
```

# Packets Handler
---
- **Server Side**<br>
Server Initialize the dictionary to handle the packets(ClientPackets) coming from client to its corresponding function in ServerHandle
```C#=1
private static void InitializeServerData()
    {
        clients = new Dictionary<int, Client>();

        for (int i = 1; i <= MaxPlayers; i++)
        {
            clients.Add(i, new Client(i));
        }

        packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)ClientPackets.LoginData, ServerHandle.CheckLoginData },
                { (int)ClientPackets.playerInput, ServerHandle.PlayerInput },
                { (int)ClientPackets.playerConvar, ServerHandle.PlayerConvar },
                { (int)ClientPackets.chatData, ServerHandle.PlayerChat },
            };
        Debug.Log("Initialized packets.");
    }
```
- **Client Side**<br>
Client Initialize the dictionary to handle the packets(ServerPackets) coming from server to its corresponding function in ClientHandle
```C#=1
private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)ServerPackets.welcome, ClientHandle.Welcome },
            { (int)ServerPackets.loginResult, ClientHandle.LoginResult },
            { (int)ServerPackets.spawnPlayer, ClientHandle.SpawnPlayer },
            { (int)ServerPackets.playerPosition, ClientHandle.PlayerPosition },
            { (int)ServerPackets.playerRotation, ClientHandle.PlayerRotation },
            { (int)ServerPackets.playerTransform, ClientHandle.PlayerTransform },
            { (int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },
            { (int)ServerPackets.serverSimulationState, ClientHandle.SimulationState },
            { (int)ServerPackets.serverConvar, ClientHandle.ServerConvar },
            { (int)ServerPackets.serverTick, ClientHandle.ServerTick },
            { (int)ServerPackets.serverCountdown, ClientHandle.ServerCountDown },
            { (int)ServerPackets.playersRole, ClientHandle.PlayersRole },
            { (int)ServerPackets.gameResult, ClientHandle.GameResult },
            { (int)ServerPackets.broadcastChat, ClientHandle.ChatReceive },
            { (int)ServerPackets.createItemSpawner, ClientHandle.CreateItemSpawner },
            { (int)ServerPackets.itemSpawned, ClientHandle.ItemSpawned },
            { (int)ServerPackets.itemPickedUp, ClientHandle.ItemPickedUp },
            { (int)ServerPackets.shieldExpired, ClientHandle.ShieldExpiration },
            { (int)ServerPackets.turboEnd, ClientHandle.TurboEnd },
            { (int)ServerPackets.gameCountdown, ClientHandle.GameCountDown },
            { (int)ServerPackets.endCountdown, ClientHandle.EndCountDown },
        };
        Debug.Log("Initialized packets.");
    }
```

# Login
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/Screenshot/Login.png)
- **Server Side**<br>
As soon as the client connected to server and the connection established, the Server will send the welcome packet to the client using welcome enum
```C#=1
public static void Welcome(int _toClient, string _msg, int _serverTick)
    {
        using (Packet _packet = new Packet((int)ServerPackets.welcome))
        {
            _packet.Write(_msg);
            _packet.Write(_toClient);
            _packet.Write(_serverTick);

            SendTCPData(_toClient, _packet);
        }
    }
```

<br> At this point the server will receive the login data and directly send the result to the corresponding client
```C#=1
public static void CheckLoginData(int _fromClient, Packet _packet)
    {
        int _clientIdCheck = _packet.ReadInt();
        string _username = _packet.ReadString();

        Debug.Log("check");
        Debug.Log($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {_fromClient}.");
        if (_fromClient != _clientIdCheck)
        {
            Debug.Log($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})!");
        }
        Debug.Log("Login");
        Server.clients[_fromClient].SendIntoGame(_username);
        Debug.Log("Data");
        ServerSend.LoginResult(_fromClient, true);
        Debug.Log("Result");
    }
    
    public static void LoginResult(int _toClient, bool _loginSuccess)
    {
        using (Packet _packet = new Packet((int)ServerPackets.loginResult))
        {
            _packet.Write(_loginSuccess);
            _packet.Write(_toClient);

            SendTCPData(_toClient, _packet);
        }
    }
```

- **Client Side**<br>
Client receive the welcome package from server and directly send the login data. After that Client will receive the login result 
```C#=1
public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();
        int _serverTick = _packet.ReadInt();

        if(GlobalVariables.clientTick != _serverTick && GlobalVariables.serverTick != _serverTick)
        {
            GlobalVariables.clientTick = _serverTick;
            GlobalVariables.serverTick = _serverTick;
        }
        Debug.Log($"Message from server: {_msg}");
        Client.instance.myId = _myId;
        ClientSend.SendLoginData();
        print("send login");

        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
        print("udp conns");
    }

public static void LoginResult(Packet _packet)
    {
        bool _loginSuccess = _packet.ReadBool();
        if (_loginSuccess)
        {
            UIManager.instance.loginResult.GetComponent<Text>().text = $"Welcome,{UIManager.instance.usernameField.text} !";
        }
        else
        {
            UIManager.instance.loginResult.GetComponent<Text>().text = $"Login Failed !";
        }

        UIManager.instance.loginSuccess = _loginSuccess;
    }
``` 


<br> Client send the login data to server via TCP
``` C#=1
public static void SendLoginData()
    {
        using (Packet _packet = new Packet((int)ClientPackets.LoginData))
        {
            _packet.Write(Client.instance.myId);
            _packet.Write(UIManager.instance.usernameField.text);
            
            SendTCPData(_packet);
        }
    }
```

# Spawn
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/GIF/Login.gif)
- **Server Side**<br>
After the server send the login result. Server will call the function to instantiate the corresponding client
```C#=1
public void SendIntoGame(string _playerName)
    {
        player = NetworkManager.instance.InstantiatePlayer();
        player.Initialize(id, _playerName);

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.id != id)
                {
                    ServerSend.SpawnPlayer(id, _client.player);
                }
            }
        }

        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                ServerSend.SpawnPlayer(_client.id, player);
            }
        }

        foreach(ItemSpawner _itemSpawner in ItemSpawner.spawners.Values)
        {
            ServerSend.CreateItemSpawner(id, _itemSpawner.spawnerId, _itemSpawner.transform.position, _itemSpawner.hasItem, _itemSpawner.spawnerType); 
        }
    }
```

<br> The function called by SendIntoGame() function to Instantiate the player prefab and get the data
```C#=1
public Player InstantiatePlayer()
    {
        return Instantiate(playerPrefab, new Vector3(i, 0, i += 2.5f), Quaternion.identity).GetComponent<Player>();
    }
```

<br> After the server get the data from instantiated player prefab, the server will send the data to client using spawnPlayer enum
```C#=1
 public static void SpawnPlayer(int _toClient, Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation);
            _packet.Write(_player.carColor.materials[1].color);
            _packet.Write(_player.carColor.materials[0].color);
            _packet.Write(_player.isZombie);
            _packet.Write(_player.shieldEnable);
            _packet.Write(_player.turboEnable);

            SendTCPData(_toClient, _packet);
        }
    }
```


- **Client Side**<br>
The client will receive the packet with spawnPlayer enum and after that Instantiate the prefab with the data contains in the packet to be the constructor
```C#=1
public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        Color _colorBody = _packet.ReadColor();
        Color _colorBumper = _packet.ReadColor();
        bool _isZombie = _packet.ReadBool();
        bool _shieldEnable = _packet.ReadBool();
        bool _turboEnable = _packet.ReadBool();


        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation, _colorBody, _colorBumper,_isZombie,_shieldEnable,_turboEnable);
    }
```
# Movement
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/Screenshot/chat.png)
- **Server Side**<br>
This is where server receive the packet coming from client containing movement input. After that the server will calculate the position based on the received input
```C#=1
 public static void PlayerInput(int _fromClient, Packet _packet)
    {
        ClientInputState inputState = new ClientInputState();

        inputState.tick = _packet.ReadInt();
        inputState.lerpAmount = _packet.ReadFloat();
        inputState.simulationFrame = _packet.ReadInt();

        inputState.buttons = _packet.ReadInt();

        inputState.HorizontalAxis = _packet.ReadFloat();
        inputState.VerticalAxis = _packet.ReadFloat();
        inputState.rotation = _packet.ReadQuaternion();

        if (!Server.clients[_fromClient].player)
            return;

        Server.clients[_fromClient].player.AddInput(inputState);
    }

```
<br> After that the server will broadcast the transform such as position and rotation to all player via UDP using playerTransform enum
```C#=1
 
    public static void PlayerTransform(Player _player)
    {
        if (!_player)
            return;

        using (Packet _packet = new Packet((int)ServerPackets.playerTransform))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation);
            _packet.Write(_player.tick);

            SendUDPDataToAll(_player.id, _packet);
        }
    }
```
- **Client Side**<br>
Client send the movement input Player Transform(Position & Rotation) to Server via UDP
```C#=1
public static void PlayerInput(ClientInputState _inputState)
    {
        using (Packet _packet = new Packet((int)ClientPackets.playerInput))
        {
            _packet.Write(_inputState.tick);
            _packet.Write(_inputState.lerpAmount);
            _packet.Write(_inputState.simulationFrame);

            _packet.Write(_inputState.buttons);
            
            _packet.Write(_inputState.HorizontalAxis);
            _packet.Write(_inputState.VerticalAxis);
            _packet.Write(_inputState.rotation);

            SendUDPData(_packet);
        }
    }
```
<br> Client receive the new Player Transform(Position & Rotation) from Server via UDP
```C#=1
public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        int _serverTick = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_serverTick > GlobalVariables.serverTick)
                GlobalVariables.serverTick = _serverTick;

            _player.interpolation.NewUpdate(_serverTick, _position);
        }
    }

public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Quaternion _rotation = _packet.ReadQuaternion();
        int _serverTick = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_serverTick > GlobalVariables.serverTick)
                GlobalVariables.serverTick = _serverTick;

            _player.interpolation.NewUpdate(_serverTick, _rotation);
        }
    }

public static void PlayerTransform(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        int _serverTick = _packet.ReadInt();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_serverTick > GlobalVariables.serverTick)
                GlobalVariables.serverTick = _serverTick;

            _player.interpolation.NewUpdate(_serverTick, _position, _rotation);
        }
    }
```
# Countdown
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/Screenshot/game%20Started.png)
- **Server Side**<br>
As soon as the minimum player to start is fulfilled, the timer will begin to countdown the timer to start the game.<br>
1.ServerCountdown() function will send the time counted by server to all clients(TCP) before the game started<br>
2.GameCountdown() function will send the time counted by server to all clients(TCP) while the game is in the playing state<br>
3.EndCountdown() function will send the time counted by server to all clients(TCP) when the game is over and need to be restarted<br>
```C#=1
public static void ServerCountdown(int timeCd)
    {
        using(Packet _packet = new Packet((int)ServerPackets.serverCountdown))
        {
            _packet.Write(timeCd);

            SendTCPDataToAll(_packet);
        }
    }

    public static void GameCountdown(int timeCd)
    {
        using (Packet _packet = new Packet((int)ServerPackets.gameCountdown))
        {
            _packet.Write(timeCd);

            SendTCPDataToAll(_packet);
        }
    }
    public static void EndCountdown(int timeCd)
    {
        using (Packet _packet = new Packet((int)ServerPackets.endCountdown))
        {
            _packet.Write(timeCd);

            SendTCPDataToAll(_packet);
        }
    }
```
- **Client Side**<br>
The client will receive the packet by its enum to call the corresponding function to show the timer in client's screen
```C#=1
public static void ServerCountDown(Packet _packet)
    {
        float _time = _packet.ReadInt();
        UIManager.instance.ShowTimeRemaining(_time);
    }
    
public static void GameCountDown(Packet _packet)
    {
        float _time = _packet.ReadInt();
        UIManager.instance.ShowGameTimeRemaining(_time);
    }
    
public static void EndCountDown(Packet _packet)
    {
        float _time = _packet.ReadInt();
        if(_time <= 0)
        {
            UIManager.instance.ReloadScene();
        }
        UIManager.instance.ShowEndTimeRemaining(_time);
    }
```

# Role Assign
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/GIF/infect.gif)
- **Server Side**<br>
Because this game is server authorative, so all colliders and game manager are handled by server. If the server detect that Human role get infected by Zombie role, the server directly send the infected player's role to all client to update the game state.
```C#=1
public static void PlayersRole(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playersRole))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.isZombie);

            SendTCPDataToAll(_packet);
        }
    }
   
```
- **Client Side**<br>
The client will receive the boolean and id of infected player to assign, so the player update the game state too.
```C#=1
public static void PlayersRole(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isZombie = _packet.ReadBool();

        GameManager.instance.AssignRole(_id, _isZombie);
    }

```

# Game Result
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/GIF/zombie%20win.gif)
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/GIF/human%20win.gif)
- **Server Side**<br>
Server authorative will detect how many human or zombie players left in the game to decide the winner. If there is no human then the zombie win. Otherwise, if the human still remain after the timer ends, then human win.<br>
The Server will send the result in bool to all client to update the game state using gameResult enum.
```C#=1
public static void GameResult(bool _result)
    {
        using(Packet _packet = new Packet((int)ServerPackets.gameResult))
        {
            _packet.Write(_result);

            SendTCPDataToAll(_packet);
        }
    }
```
- **Client Side**<br>
The client will receive the boolean to assign the winner and show the winner in client's screen
```C#=1
public static void GameResult(Packet _packet)
    {
        bool _result = _packet.ReadBool();

        if (!_result)
        {
            UIManager.instance.gameResult.GetComponent<Text>().text = "Zombie Win";
            UIManager.instance.gameResult.GetComponent<Text>().color = new Color(0, 1, 0);
        }
        else
        {
            UIManager.instance.gameResult.GetComponent<Text>().text = "Human Win";
            UIManager.instance.gameResult.GetComponent<Text>().color = new Color(0, 0, 1);
        }
    }
```
# Chat System
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/GIF/Chat.gif)
- **Server Side**<br>
The server will receive the packet with chatData enum from the client. The Packet will contain the ID of sender and the message.
```C#=1
public static void PlayerChat(int _fromClient, Packet _packet)
    {
        string _msg = _packet.ReadString();
        string _name = Server.clients[_fromClient].player.username;


        ServerSend.Chat(_name, _msg);
        Debug.Log(_name + " : " + _msg);
    }
```

<br> After that the server directly broadcast the chat to all client using broadcastChat enum via TCP
```C#=1
public static void Chat(string _sender, string _msg)
    {
        using (Packet _packet = new Packet((int)ServerPackets.broadcastChat))
        {
            _packet.Write(_sender);
            _packet.Write(_msg);

            SendTCPDataToAll(_packet);
        }
    }
```
- **Client Side**<br>
Client send the chat to Server via TCP using chatData enum
```C#=1
public static void PlayerChat()
    {
        using (Packet _packet = new Packet((int)ClientPackets.chatData))
        {
            _packet.Write(UIManager.instance.chatField.text);

            SendTCPData(_packet);
            UIManager.instance.chatField.text = null;
        }
    }
```
<br> Client Receive the chat from Server and quickly instantiate the message to be shown in the chat box
```C#=1
public static void ChatReceive(Packet _packet)
    {
        string _name = _packet.ReadString();
        string _msg = _packet.ReadString();

        UIManager.instance.ShowBroadcast(_name, _msg);
    }
```
# Item Spawner
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/Screenshot/4%20people%20not%20start.png)
- **Server Side** <br>
As soon as the client instantiated, the server also instantiate the item spawner in the server. After that the server will get the data about the item spawner and then send it to the client using createItemSpawner enum via TCP. The server also send the spawnerid and the spawnerType to each client
```C#=1
public static void CreateItemSpawner(int _toClient, int _spawnerId, Vector3 _spawnerposition, bool _hasItem, int _spawnerType)
    {
        using (Packet _packet = new Packet((int)ServerPackets.createItemSpawner))
        {
            _packet.Write(_spawnerId);
            _packet.Write(_spawnerposition);
            _packet.Write(_hasItem);
            _packet.Write(_spawnerType);

            SendTCPData(_toClient, _packet);
        }
    }

    public static void ItemSpawned(int _spawnerId, int _spawnerType)
    {
        using (Packet _packet = new Packet((int)ServerPackets.itemSpawned))
        {
            _packet.Write(_spawnerId);
            _packet.Write(_spawnerType);

            SendTCPDataToAll(_packet);
        }
    }
```
- **Client Side**<br>
The Client will receive the position, boolean, id and type of spawner from server to be the constructor to instantiate the item spawner in the clients game. The client also receive the id and type of spawner that will be assigned in the item spawner.
```C#=1
public static void CreateItemSpawner(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        Vector3 _spawnerPosition = _packet.ReadVector3();
        bool _hasItem = _packet.ReadBool();
        int _spawnerType = _packet.ReadInt();

        GameManager.instance.CreateItemSpawner(_spawnerId, _spawnerPosition, _hasItem, _spawnerType);
    }
    
public static void ItemSpawned(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        int _spawnerType = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemSpawned(_spawnerType);
    }
```
# Pick Up Power Up
---
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/GIF/take%20item.gif)
- **Server Side** <br>
Server authorative will check whether the item in itemSpawner has been picked up by player or not. If the item has been picked up then the server will broadcast the spawnerId, spawnerType, and player who picked it up to all client using itemPickedUp enum via TCP
```C#=1
 public static void ItemPickedUp(int _spawnerId, int _spawnerType, int _byPlayer)
    {
        using (Packet _packet = new Packet((int)ServerPackets.itemPickedUp))
        {
            _packet.Write(_spawnerId);
            _packet.Write(_spawnerType);
            _packet.Write(_byPlayer);

            SendTCPDataToAll(_packet);
        }
    }
```
- **Client Side**<br>
The client will receive the boolean so the item in item spawner will disappear based on spawnerId and spawnerType. The game manager will assign the player who just pick the item.
```C#=1
public static void ItemPickedUp(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        int _spawnerType = _packet.ReadInt();
        int _byPlayer = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemPickedUp();
        GameManager.players[_byPlayer].PowerUp(_spawnerType);
    }
```

# Shield Expiration
---
- **Server Side**<br>
Server authorative will check and count the time of shield expiration using co-routine. The shield expiration is 10 seconds. When the shield is activated, the server will make the player vulnerable for 10 seconds and after that the server will send the boolean of a player to be false.
```C#=1
public static void ShieldExpiration(Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.shieldExpired))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.shieldEnable);

            SendTCPDataToAll(_packet);
        }
    }
```
- **Client Side**<br>
When the client's shield is expired the client will receive a new boolean coming from server to be assigned to corresponding player. 
```C#=1
public static void ShieldExpiration(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _shieldEnable = _packet.ReadBool();

        GameManager.players[_id].shieldEnable = _shieldEnable;
    }
```

