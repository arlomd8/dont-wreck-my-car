Don't Wreck My Car
===
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/cover.png)


# Introduction
Hello my name is [Arlo Mario Dendi](https://gitlab.com/arlomd8). Welcome to this project. This project was aimed to create and build online multiplayer game. I started this project with my friend  [Dicky Dwi Darmawan](https://gitlab.com/ddwi16) to fulfill the final project of Desain Game Multiplayer Online course. The game is using TCP or/and UDP, native network programming, and able to play online. The game also can be played in Desktop(Windows).

# Table Of Contents
* [Introduction](#introduction)
* [General Info](#general-info)
* [Technologies](#technologies)
* [Install and Play](#install-and-play)
* [Other Documents](#other-documents)
* [Data Transfer](#data-transfer)
* [Gameplay](#gameplay)
* [How The Code Works](#how-the-code-works)
* [Credits](#credits)

# General Info
The game will let you to play as a car to survive as a human until the timer ends or to infect as a zombie before the timer ends. There's also two items to cure the zombie infection and to make player vulnerable from zombie infection. 
<br>
<br> This is the **feature** that availables in the game :
1. Login System
2. Chat System
3. Ability System
4. Different Car Color

# Technologies
This game created with : 
- Unity 2019.4.11f1 (LTS)
- Google Cloud Platform
<br>
The game(client) will be build on Unity and the server will be hosted in Google Cloud Platform

# Install and Play
How to install the game and Play the game
1. Visit [itch.io](https://tetanggabaikdev.itch.io/dont-wreck-my-car) to download the game
2. Extract the .rar file
3. Play the **ClientDWMC.exe**
4. Enjoy the game!

# Other Documents
1. You can access the High Concept Statement in [here](https://docs.google.com/presentation/d/1ODDbW2rSJmP9c4XE5I2si7ifleiITmVkRr2hbQJm918/edit?usp=sharing)
2. You can access the Game Design Document in [here](https://docs.google.com/document/d/1eTlKIkL03ffgYK61gvhXmHtN-yhvru7UfjfwePa9CxU/edit?usp=sharing)

# Data Transfer
The following data will be transferred in this Protocol :
* **TCP** 
    * Welcome Packet    
    * Login Data
    * Login Result
    * Spawn Player (Position, Rotation, Id, Nickname, Car Color, Ability)
    * Timer Countdown
    * Player's Role (Zombie/Human)
    * Game Result
    * Chat Data
    * Spawning Item Spawner
    * Spawned Item data
    * Item Picked Up
    * Shield Expiration
    <br>
* **UDP**
    * Player Transform
    * Player Position
    * Player Rotation 

# Gameplay
This is the gameplay overview (wait for the .gif to load)<br>
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/GIF/full%20gameplay.gif)
<br>
You can also see the screenshots of the game in [here](https://gitlab.com/arlomd8/dont-wreck-my-car/-/tree/main/Screenshot)

# How The Code Works
![](https://gitlab.com/arlomd8/dont-wreck-my-car/-/raw/main/Screenshot_1.png)
<br>
The following point will explain how the code works :
* [Packets](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#packets)
* [Packet Handler](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#packets-handler)
* [Login](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#login)
* [Spawn](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#spawn)
* [Movement](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#movement)
* [Countdown](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#countdown)
* [Role Assign](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#role-assign)
* [Game Result](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#game-result)
* [Chat System](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#chat-system)
* [Item Spawner](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#item-spawner)
* [Pick Up Power Up](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#pick-up-power-up)
* [Shield Expiration](https://gitlab.com/arlomd8/dont-wreck-my-car/-/blob/main/CodeWorks.md#shield-expiration)

# Credits
Thanks to : 
1. [Dicky Dwi Darmawan](https://gitlab.com/ddwi16)
2. [Tom Weiland](https://www.youtube.com/channel/UCa-mDKzV5MW_BXjSDRqqHUw) 
3. [Kenney](https://kenney.nl)
